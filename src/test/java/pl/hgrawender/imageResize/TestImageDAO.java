package pl.hgrawender.imageResize;

import static org.junit.Assert.assertNotNull;

import org.junit.AfterClass;
        import org.junit.BeforeClass;



///**
// * Test Case
// *
// * @author Loiane Groner
// * http://loianegroner.com (English)
// * http://loiane.com (Portuguese)
// */
//public class TestImageDAO {
//
//    private static ImageDAOImpl imageDAO;
//
//    @BeforeClass
//    public static  void runBeforeClass() {
//        imageDAO = new ImageDAOImpl();
//    }
//
//    @AfterClass
//    public static void runAfterClass() {
//        imageDAO = null;
//    }
//
//    /**
//     * Test method for {@link pl.hgrawender.imageResize.Repositories.ImageDAOImpl#saveImage()}.
//     */
//    @Test
//    public void testSaveImage() {
//
//        //File file = new File("static/car.jpg"); //windows
//        File file = new File("E:\\JAVA\\imageResize\\src\\main\\resources\\static\\car.jpg"); //windows
//
//        //File file = new File("images\\extjsfirstlook.jpg"); //windows
//        //File file = new File("images/extjsfirstlook.jpg");
//        byte[] bFile = new byte[(int) file.length()];
//
//        try {
//            FileInputStream fileInputStream = new FileInputStream(file);
//            fileInputStream.read(bFile);
//            fileInputStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        UpImage image = new UpImage();
//        image.setName("Ext JS 4 First Look");
//        image.setImage(bFile);
//
//        ImageRepository.save(image);
//
//        assertNotNull(image.getId());
//    }

//    /**
//     * Test method for {@link pl.hgrawender.imageResize.Repositories.ImageDAOImpl#getImage()}.
//     */
//    @Test
//    public void testGetImage() {
//
//        UpImage image = imageDAO.getImage((long) 1);
//
//        assertNotNull(image);
//
//        try{
//            //FileOutputStream fos = new FileOutputStream("images\\output.jpg");  //windows
//            FileOutputStream fos = new FileOutputStream("static/output.jpg");
//            fos.write(image.getImage());
//            fos.close();
//        }catch(Exception e){
//            e.printStackTrace();
//        }
//    }
//}