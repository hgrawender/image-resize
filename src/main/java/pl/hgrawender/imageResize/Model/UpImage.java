package pl.hgrawender.imageResize.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Date;


@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(name = "images")
public class UpImage  {

    @JsonProperty
    @Id
    @GeneratedValue
    @Column(name="IMAGE_ID")
    private long id;

    @JsonProperty
    @Column(name="IMAGE_NAME", nullable=false)
    private String name;

    @JsonProperty
    @Lob
    @Column(name="IMAGE", nullable=false, columnDefinition="mediumblob")
    private byte[] image;

    @JsonProperty
    @Lob
    @Column(name="IMAGE_MODIFIED", columnDefinition="mediumblob")
    private byte[] imageModified;

    //Used to store when image was uploaded
    @JsonProperty
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="DATE")
    private java.util.Calendar date;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public byte[] getImageModified() {
        return imageModified;
    }

    public void setImageModified(byte[] imageModified) {
        this.imageModified = imageModified;
    }

    public java.util.Calendar getDate() {
        return date;
    }

    public void setDate(java.util.Calendar date) {
        this.date = date;
    }
}
