package pl.hgrawender.imageResize.Utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

public class TransformImage {

    //Mirror image saved as BufferedImage
    public static BufferedImage mirror(BufferedImage originalImage) {

        int width = originalImage.getWidth();
        int height = originalImage.getHeight();
        BufferedImage modifiedImage = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < height; y++) {
            for (int lx = 0, rx = width - 1; lx < width; lx++, rx--) {
                int p = originalImage.getRGB(lx, y);
                modifiedImage.setRGB(rx, y, p);
            }
        }

        System.out.println("Mirror (BufferedImage) has been completed successfully!");
        return modifiedImage;
    }


    //Mirror image saved as byte array
    public static byte[] mirrorFromByte(byte[] bimage) {

        BufferedImage simg = null;
        try {
            InputStream in = new ByteArrayInputStream(bimage);
            simg = ImageIO.read(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

        int width = simg.getWidth();
        int height = simg.getHeight();
        BufferedImage mimg = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < height; y++) {
            for (int lx = 0, rx = width - 1; lx < width; lx++, rx--) {
                int p = simg.getRGB(lx, y);
                mimg.setRGB(rx, y, p);
            }
        }

        byte[] imageInByte = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write( mimg, "jpg", baos );
            baos.flush();
            imageInByte = baos.toByteArray();
            baos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Mirror (byte[]) has been completed successfully!");
        return imageInByte;
    }
}
