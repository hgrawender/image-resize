package pl.hgrawender.imageResize.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import pl.hgrawender.imageResize.Model.UpImage;
import pl.hgrawender.imageResize.Repositories.ImageRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Controller
@Component
public class ImagesController {

    private ImageRepository imageRepository;
    @Autowired(required = true)
    public void setImageRepository(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    //Deleting one image by id
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public void delete(@PathVariable(value = "id") Long id) {

        try {
            imageRepository.deleteById(id);
            System.out.println("Success deleting an image image with id: "+id);
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    //Getting one image by id
    @RequestMapping(value = "/getImage/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> getImage(@PathVariable(value = "id") Long id,
                                           HttpServletRequest request, HttpServletResponse response) {

        byte[] bFile = null;
        try {
            UpImage foundImage = imageRepository.getOne(id);
            String name = foundImage.getName();
            bFile = foundImage.getImage();
            System.out.println("Success getting image with name: "+name);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(bFile);
    }


    //Getting one modified version of image by id
    @RequestMapping(value = "/getImageModified/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> getImageModified(@PathVariable(value = "id") Long id, HttpServletRequest request, HttpServletResponse response) {

        byte[] bFile = null;
        try {
            UpImage foundImage = imageRepository.getOne(id);
            String name = foundImage.getName();
            bFile = foundImage.getImageModified();
            System.out.println("Success getting  modified image with name: "+name);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(bFile);
    }


    //Getting JSON with modified version of image by id
    @RequestMapping(value = "/getImageJson/{id}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String getImage2(@PathVariable(value = "id") Long id, HttpServletRequest request, HttpServletResponse response) {

        UpImage foundImage = null;
        try {
            foundImage = imageRepository.getOne(id);
        } catch (Exception e) {
            e.printStackTrace();
        }


        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        try {
            jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(foundImage);
            System.out.println("Success getting JSON with modified image");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonInString;
    }


    //Getting all images as JSON with all details such as id, name, timestamps etc.
    @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String getImage2(HttpServletRequest request, HttpServletResponse response) {

        List<UpImage> foundImages = new ArrayList<>();

        try {
            foundImages = imageRepository.findAll() ;
        } catch (Exception e) {
            e.printStackTrace();
        }

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;

        try {
            jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(foundImages);
            System.out.println("Success getting data about all images!");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonInString;
    }


    //Add one image with POST
    @RequestMapping(value = "/addImage", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.OK)
    public String uploadImage(@RequestParam MultipartFile file, HttpServletRequest request, HttpServletResponse response) {

        try {
            System.out.println("Saving file: " + file.getOriginalFilename());
            byte[] bFile = file.getBytes();
            UpImage image = new UpImage();
            image.setName(file.getOriginalFilename());
            Calendar now = Calendar.getInstance();
            image.setDate(now);
            image.setImage(bFile);
            imageRepository.save(image);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "Success saving image with POST";
    }

}
