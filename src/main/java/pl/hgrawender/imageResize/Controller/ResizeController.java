package pl.hgrawender.imageResize.Controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import pl.hgrawender.imageResize.Model.UpImage;
import pl.hgrawender.imageResize.Utils.TransformImage;
import pl.hgrawender.imageResize.Repositories.ImageRepository;

import javax.imageio.ImageIO;
import java.awt.*;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Calendar;
import java.util.List;

@Controller
@Component
public class ResizeController {

    private ImageRepository imageRepository;
    @Autowired(required = true)
    public void setImageRepository(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }



    @RequestMapping(value = "/ResizeImageJSON", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public String resImageJson(@RequestParam MultipartFile file, @RequestParam(required = false, defaultValue="false") boolean mirror,
                         @RequestParam(required = false) int desiredWidth,
                         @RequestParam(required = false) int desiredHeight,
                         @RequestParam(required = false) String desiredFilename,HttpServletRequest request,HttpServletResponse response) throws Exception {

        System.out.println("width: "+ desiredWidth);
        System.out.println("height: "+ desiredHeight);

        UpImage image = new UpImage();
        byte[] bFile = null;
        bFile = file.getBytes();


        InputStream in = new ByteArrayInputStream(bFile);
        BufferedImage originalImage = ImageIO.read(in);


        BufferedImage modifiedImage = originalImage;
        if(mirror == true) {
            modifiedImage = TransformImage.mirror(originalImage);
        }

        ByteArrayOutputStream baosModifiedImage = new ByteArrayOutputStream();
        try {
            Image newImage = modifiedImage.getScaledInstance(desiredWidth, desiredHeight, Image.SCALE_SMOOTH);
            BufferedImage resizedImage = new BufferedImage(desiredWidth, desiredHeight, BufferedImage.TYPE_INT_RGB);
            resizedImage.getGraphics().drawImage(newImage, 0, 0, new Color(0,0,0), null);
            ImageIO.write(resizedImage, "jpg", baosModifiedImage);
        } catch (IllegalArgumentException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }


        byte[] imgByte = baosModifiedImage.toByteArray();
        try {
           // System.out.println("Saving image in DB: " + file.getOriginalFilename());
            if(desiredFilename != null && !desiredFilename.isEmpty()) {
                image.setName(desiredFilename);
            } else {
                image.setName(file.getOriginalFilename());
            }

            Calendar now = Calendar.getInstance();
            image.setDate(now);
            image.setImage(bFile);
            image.setImageModified(imgByte);
            imageRepository.save(image);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Success getting image!");

        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        try {
            jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(imgByte);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return jsonInString;

    }


    @RequestMapping(value = "/MirrorImage", method = RequestMethod.POST)
    public void showImage(@RequestParam MultipartFile file, @RequestParam(required = false, defaultValue = "false") boolean mirror, HttpServletRequest request,HttpServletResponse response) throws Exception {

        System.out.println("mirror: "+mirror);

        byte[] bFile = null;
        try {
            System.out.println("Saving file: " + file.getOriginalFilename());
            bFile = file.getBytes();
            UpImage image = new UpImage();
            image.setName(file.getOriginalFilename());
            Calendar now = Calendar.getInstance();
            image.setDate(now);
            image.setImage(bFile);
            imageRepository.save(image);

        } catch (IOException e) {
            e.printStackTrace();
        }

        ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();
        UpImage imageObject = imageRepository.getOne((long) 1);

        byte[] mImage = bFile;
        if(mirror == true) {
            mImage = TransformImage.mirrorFromByte(bFile);
        }

        try {
            InputStream in = new ByteArrayInputStream(mImage);
            BufferedImage image = ImageIO.read(in);

            ImageIO.write(image, "jpeg", jpegOutputStream);
        } catch (IllegalArgumentException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }

        byte[] imgByte = jpegOutputStream.toByteArray();

        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("image/jpeg");
        ServletOutputStream responseOutputStream = response.getOutputStream();
        responseOutputStream.write(imgByte);
        responseOutputStream.flush();
        responseOutputStream.close();
    }

    @RequestMapping(value = "/ResizeImage", method = RequestMethod.POST)
    public void resImage(@RequestParam MultipartFile file, @RequestParam(required = false, defaultValue="false") boolean mirror,
                         @RequestParam(required = false) int desiredWidth,
                         @RequestParam(required = false) int desiredHeight,
                         @RequestParam(required = false) String desiredFilename,HttpServletRequest request,HttpServletResponse response) throws Exception {

        System.out.println("width: "+ desiredWidth);
        System.out.println("height: "+ desiredHeight);

        UpImage image = new UpImage();
        byte[] bFile = null;
        bFile = file.getBytes();


        InputStream in = new ByteArrayInputStream(bFile);
        BufferedImage originalImage = ImageIO.read(in);


        BufferedImage modifiedImage = originalImage;
        if(mirror == true) {
            modifiedImage = TransformImage.mirror(originalImage);
        }

        ByteArrayOutputStream baosModifiedImage = new ByteArrayOutputStream();
        try {
            Image newImage = modifiedImage.getScaledInstance(desiredWidth, desiredHeight, Image.SCALE_SMOOTH);
            BufferedImage resizedImage = new BufferedImage(desiredWidth, desiredHeight, BufferedImage.TYPE_INT_RGB);
            resizedImage.getGraphics().drawImage(newImage, 0, 0, new Color(0,0,0), null);
            ImageIO.write(resizedImage, "jpg", baosModifiedImage);
            //ImageIO.write(modifiedImage, "jpeg", jpegOutputStream);
        } catch (IllegalArgumentException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
        }


        byte[] imgByte = baosModifiedImage.toByteArray();
        try {
            //System.out.println("Saving image in DB: " + file.getOriginalFilename());

            if(desiredFilename != null && !desiredFilename.isEmpty()) {
                image.setName(desiredFilename);
            } else {
                image.setName(file.getOriginalFilename());
            }


            Calendar now = Calendar.getInstance();
            image.setDate(now);
            image.setImage(bFile);
            image.setImageModified(imgByte);
            imageRepository.save(image);
        } catch (Exception e) {
            e.printStackTrace();
        }


        response.addHeader("Access-Control-Allow-Methods", "OPTIONS"); // also added header to allow POST, GET method to be available
        response.addHeader("Access-Control-Allow-Headers", "OPTIONS");
        response.addHeader("Access-Control-Allow-Origin", "*"); // also added header to allow cross domain request for any domain
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("image/jpg");
        ServletOutputStream responseOutputStream = response.getOutputStream();
        responseOutputStream.write(imgByte);
        responseOutputStream.flush();
        responseOutputStream.close();

    }




}



