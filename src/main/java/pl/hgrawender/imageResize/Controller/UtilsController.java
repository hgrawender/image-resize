package pl.hgrawender.imageResize.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pl.hgrawender.imageResize.Model.UpImage;
import pl.hgrawender.imageResize.Repositories.ImageRepository;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

@Controller
@Component
public class UtilsController {

    private ImageRepository imageRepository;
    @Autowired(required = true)
    public void setImageRepository(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    //This file consists addition methods written during development, that are not currently used in final product
    //Add image to database via path
    @RequestMapping(value="/addImageviaPath", method = RequestMethod.GET)
    @ResponseBody
    public String addImage(){

        File file = new File("E:\\JAVA\\imageResize\\src\\main\\resources\\static\\car.jpg");
        byte[] bFile = new byte[(int) file.length()];

        UpImage image = new UpImage();
        image.setName("Image_name");
        image.setImage(bFile);
        imageRepository.save(image);

        return "Success in adding image to Database!";
    }


    //Get image from database to the given folder
    @RequestMapping(value="/getImageToPath", method = RequestMethod.GET)
    @ResponseBody
    public String GetImageMirror() throws IOException {

        UpImage image = imageRepository.getOne((long) 1);
        InputStream in = new ByteArrayInputStream(image.getImage());
        BufferedImage simg = ImageIO.read(in);

        int width = simg.getWidth();
        int height = simg.getHeight();
        BufferedImage mimg = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        for (int y = 0; y < height; y++) {
            for (int lx = 0, rx = width - 1; lx < width; lx++, rx--) {
                int p = simg.getRGB(lx, y);
                mimg.setRGB(rx, y, p);
            }
        }

        try {
            ImageIO.write(mimg, "jpg", new File("E:\\JAVA\\imageResize\\src\\main\\resources\\static\\car4mirror.jpg"));
        } catch(IOException e) {
            System.out.println("Error: " + e);
        }
        return "Success getting image in place!";
    }
}
